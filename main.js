// Initialize an empty array to store the cap table data
let capTableData = [];

// Function to add an investor
function addInvestor() {
  // Get the input values
  let name = document.querySelector("#investorName").value;
  let investment = document.querySelector("#investmentAmount").value;
  let postMoneyValuation = document.querySelector("#postMoneyValuation").value;

  // Validate the inputs
  if (
    !name ||
    !investment ||
    !postMoneyValuation ||
    investment <= 0 ||
    postMoneyValuation <= 0
  ) {
    alert("Please enter valid inputs");
    return;
  }

  // Calculate the equity
  let equity = (investment / postMoneyValuation) * 100;

  // Add the investor to the cap table data
  capTableData.push({
    name: name,
    role: "Investor",
    equity: equity,
  });

  // Update the cap table
  updateCapTable();
}

// Function to add a founder
function addFounder() {
  // Get the input values
  let name = document.querySelector("#founderName").value;
  let equity = parseFloat(document.querySelector("#founderEquity").value);

  // Validate the inputs
  if (!name || !equity || equity <= 0) {
    alert("Please enter valid inputs");
    return;
  }

  // Add the founder to the cap table data
  capTableData.push({
    name: name,
    role: "Founder",
    equity: equity,
  });

  // Update the cap table
  updateCapTable();
}

// Function to update the cap table
function updateCapTable() {
  // Get the cap table body element
  let capTableBody = document.querySelector("#capTableBody");

  // Clear the cap table body
  capTableBody.innerHTML = "";

  // Add each person in the cap table data to the cap table body
  for (let i = 0; i < capTableData.length; i++) {
    let person = capTableData[i];
    let row = document.createElement("tr");
    row.innerHTML = `
            <td class="border p-2">${person.name}</td>
            <td class="border p-2">${person.role}</td>
            <td class="border p-2">${person.equity.toFixed(2)}%</td>
            <td class="border p-2"><button onclick="removeRow(${i})">Remove</button></td>
        `;
    capTableBody.appendChild(row);
  }
}

// Function to remove a row from the cap table
function removeRow(index) {
  // Remove the person from the cap table data
  capTableData.splice(index, 1);

  // Update the cap table
  updateCapTable();
}

// Add event listeners to the add investor and add founder buttons
document
  .querySelector("#addInvestorButton")
  .addEventListener("click", addInvestor);
document
  .querySelector("#addFounderButton")
  .addEventListener("click", addFounder);
